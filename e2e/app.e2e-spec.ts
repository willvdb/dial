import { DialPage } from './app.po';

describe('dial App', () => {
  let page: DialPage;

  beforeEach(() => {
    page = new DialPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
