const pg = require('pg');
const bcrypt = require('bcryptjs');
const csv = require('csvtojson');
const json2csv = require('json2csv');
const csvFilePath = './transactions.csv';
const _ = require('lodash');
const config = require('./db_config');
const pool = new pg.Pool(config);

const createLeedsTable= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('CREATE TABLE leeds (' +
	        'id SERIAL PRIMARY KEY,' +
	        'business character varying(80) references businesses(name),' +
	        'assigned_user int references users(id),' +
	        'code int,' +
					'phone character varying(80) UNIQUE,' +
	        'date_created date,' +
			 		'campaign character varying(80),' +
	        'data jsonb' +
		');', function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			}

			cb(null, res);
		});
	});
}

const createBusinessTable= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('CREATE TABLE businesses ( name character varying(80) PRIMARY KEY, data jsonb);', function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			}

			cb(null, res);
		});
	});
}

const createUsersTable= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('CREATE TABLE users (id SERIAL PRIMARY KEY, username character varying(80) UNIQUE, hash character varying(80), business character varying(80) references businesses(name), data jsonb);', function(err, res){
			if (err){
				console.log('error');
				done(cb(JSON.stringify(err), null))
			} else {
				console.log('no error');
				done(cb(null, res));
			}

		});
	});
}

const insertBradyCompany= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('INSERT INTO businesses (name, data) VALUES ($1, $2);', ['brady', {
			"appSID": "AP4d50edba87007d45e77c63e5ad48d03a",
		    "twilioSID": "AC4b2d67618a4fe0081acca32c024af682",
		    "twilioToken": "3c1e9b60610c148e05996b9518661060"
		}], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			} else {
				done(cb(null, res));
			}
		});
	});
}

const insertTestCompany= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('INSERT INTO businesses (name, data) VALUES ($1, $2);', ['test', {
			"appSID": "AP4d50edba87007d45e77c63e5ad48d03a",
		    "twilioSID": "AC4b2d67618a4fe0081acca32c024af682",
		    "twilioToken": "3c1e9b60610c148e05996b9518661060"
		}], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			} else {
				done(cb(null, res));
			}
		});
	});
}

const updateBass= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('UPDATE businesses SET data=$1 WHERE name=$2;', [{
			"appSID": "AP4d50edba87007d45e77c63e5ad48d03a",
		    "twilioSID": "AC4b2d67618a4fe0081acca32c024af682",
		    "twilioToken": "3c1e9b60610c148e05996b9518661060"
		}, 'bass'], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			} else {
				done(cb(null, res));
			}
		});
	});
}

const getBusinessData = function(businessName, cb){
	pool.connect(function(err, client, done){
		if (err){
			done()
			cb(err, null);
		}

		let query = 'SELECT data FROM businesses WHERE name=$1;';

		client.query(query, [businessName], function(err, res){
			if (err){
				done(err)
				cb(err, null);
			}
			done()
			cb(null, JSON.stringify(res.rows));
		});
	});
}

const getCompanies= function(cb){
	pool.connect(function(err, client, done){
		if (err){
			done();
			cb(err, null);
		}

		client.query('SELECT * FROM businesses', function(err, res){
			if (err){
				done();
				cb(JSON.stringify(err), null);
			} else {
				done(cb(null, res));
			}

		});
	});
}

const addUser = function(username, hash, business, userData, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('INSERT INTO users (username, hash, business, data) VALUES ($1, $2, $3, $4);', [username, hash, business, userData], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			} else {
				done(cb(null, res));
			}
		});
	});
}

const getUser = function(username, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null);
		}

		client.query('SELECT * FROM users WHERE username=$1', [username], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			} else {
				if (res.rowCount === 1){
					console.log(JSON.stringify(res.rows[0]))
					cb(null, JSON.stringify(res.rows[0]));
				} else {
					cb('username does not exist', null);
				}
			}

		});
	});
}



let userData = JSON.stringify({
	"role": "admin",
	"email": "test@test.com",
	"lastName": "McGee",
	"firstName": "Bob",
	"number": ""
});
let salt = bcrypt.genSaltSync(10);
let hash = bcrypt.hashSync('test', salt);

createBusinessTable((err,res) => {
	if (err){
		console.log(err);
	} else {
		console.log(res);
		createUsersTable((err,res) => {
			if (err){
				console.log(err);
			} else {
				console.log(res);
				createLeedsTable((err,res) => {
					if (err){
						console.log(err);
					} else {
						console.log(res);
						insertBradyCompany((err,res) => {
							if (err){
								console.log(err);
							} else {
								console.log(res);
								insertTestCompany((err,res) => {
									if (err){
										console.log(err);
									} else {
										console.log(res);
										addUser('test', hash, 'test', userData, (err,res) => {
											if (err){
												console.log(err);
											} else {
												console.log(res);
											}
										})
									}
								})
							}
						})
					}
				})
			}
		})
	}
})
// getUser('test', (err, res) => {
// 	if (err){
// 		console.error(err);
// 	} else {
// 		console.log(res);
// 		console.log(pool);
// 	}
// });
