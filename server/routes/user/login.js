const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const pg = require('pg');
const bcrypt = require('bcryptjs');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const getUser = function(username, cb){
	pool.connect(function(err, client, done){
		console.log('connected');
		if (err){
			done()
			cb(err, null);
		}

		client.query('SELECT * FROM users WHERE username=$1', [username], function(err, res){
			console.log('completed query');
			if (err){
				done(err)
				cb(err, null);
			}

			if (res.rowCount === 1){
				done()
				cb(null, JSON.stringify(res.rows[0]));
			} else {
				done()
				cb('username does not exist', null);
			}
		});
	});
}



module.exports = (req, res) => {

	let username = req.body.username,
		password = req.body.password;

	console.log('recieved');

	getUser(username, (err, result) => {
		console.log('sending response');
		if (err){
			res.json({
				success: 0,
				code: 2,
				msg: 'invalid username'
			});
		} else {
			let user = JSON.parse(result);
			if (bcrypt.compareSync(password, user.hash)){
				let newToken = jwt.sign({username: req.body.username}, secret);
				res.json({
					success: 1,
					token: newToken,
					user: user
				});
			} else {
				res.json({
					success: 0,
					code: 3,
					msg: 'invalid password'
				});
			}
		}
	});
}
