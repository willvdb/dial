const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const stripeTestPrivateKey = require('../../api_config').stripeTestPrivateKey;
const pg = require('pg');
const bcrypt = require('bcryptjs');
const config = require('../../db_config');
const stripe = require('stripe')(stripeTestPrivateKey);
const pool = new pg.Pool(config);

const addUser = function(username, hash, business, userData, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('INSERT INTO users (username, hash, business, data) VALUES ($1, $2, $3, $4);', [username, hash, business, userData], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			}

			cb(null, JSON.stringify(res));
		});
	});
}

module.exports = (req, res) => {

	let username = req.body.username,
		salt = bcrypt.genSaltSync(10),
		hash = bcrypt.hashSync(req.body.password, salt),
		business = req.body.business,
		customerID = '',
		data = req.body.data;

	// stripe.customers.create({
	// 	email: data.email
	// }).then((customer) => {
	// 	customerID = customer.id
	// 	return stripe.customers.createSource(customerID, {
	// 		source: {
	// 			object: 'card',
	// 			exp_month: ccExpMonth,
	// 			exp_year: ccExpYear,
	// 			number: ccNumber,
	// 			cvc: ccCVV
	// 		}
	// 	});
	// }).then((source) => {
	// 	return stripe.customers.createSubscription(customerID, {
	// 		plan: 'sapphire-beginner-252'
	// 	})
	// }).then((result) => {
	// 	console.log(result);
	// }).catch((err) => {
	// 	console.log(err);
	// })

	addUser(username, hash, business, data, function(err, result){
		if (err){
			let error = JSON.parse(err);
			if (err.code === '23505') {
				res.json({
					success: 0,
					code: 1,
					msg: 'that username is already taken',
					error: error
				})
			} else {
				console.log(err);
				res.json({
					success: 0,
					code: 2,
					msg: 'unknown database error',
					error: error
				})
			}
		} else {
			res.json({
				success: 1,
				username: username,
				hash: hash
			})
		}
	});
}
