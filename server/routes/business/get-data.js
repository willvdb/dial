const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const url = require('url');
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const getBusinessData = function(businessName, cb){
	pool.connect(function(err, client, done){
		if (err){
			done()
			cb(err, null);
		}

		let query = 'SELECT data FROM businesses WHERE name=$1;';

		client.query(query, [businessName], function(err, res){
			if (err){
				done(err)
				cb(err, null);
			}
			done()
			cb(null, JSON.stringify(res.rows[0]));
		});
	});
}

module.exports = (req, res) => {
	let url_parts = url.parse(req.url, true);
	let businessName = url_parts.query.business;
	let token = req.headers.authorization;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).send('invalid token');
		}else{
			getBusinessData(businessName, (err, result) => {
				if (err){
					res.status(500).send(err);
				}

				res.status(200).json(result);
			});
		}
	});
}
