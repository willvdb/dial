const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const updateUser = function(businessName, businessData, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null);
		}
		let query1 = 'UPDATE businesses SET data=\'' + JSON.stringify(businessData).split('\'').join('') + '\' WHERE name=\'' + businessName + '\';';
		let query = 'UPDATE businesses SET data=$1 WHERE name=$2;';

		client.query(query1, function(err, res){
			if (err){
				cb(err, null);
			} else {
				cb(null, JSON.stringify(res.rows));
			}
		});
	});
}



module.exports = (req, res) => {
	let token = req.body.token;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).json({
				err: 'invalid token',
				success: false
			});
		}else{
			let businessName = req.body.name,
				businessData = req.body.data;

			updateUser(businessName, businessData, (err, result)=>{
				if (err){
					res.status(500).json({
						err: err,
						success: false
					});
				} else {
					res.status(200).json({
						result: result,
						success: true
					});
				}

			});
		}
	});
}
