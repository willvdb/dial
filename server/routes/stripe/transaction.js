const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const twilioToken = require('../../api_config').twilioToken;
const twilioSID = require('../../api_config').twilioSID;
const appSID = require('../../api_config').appSID;
const ClientCapability = require('twilio').jwt.ClientCapability;

const generateToken = function(identity) {
	const capability = new ClientCapability({
		accountSid: twilioSID,
		authToken: twilioToken
	});

	capability.addScope(new ClientCapability.IncomingClientScope(identity));
	capability.addScope(new ClientCapability.OutgoingClientScope({
		applicationSid: appSID,
		clientName: identity
	}));

	return {
		identity: identity,
		token: capability.toJwt()
	};
}


module.exports = (req, res) => {
	let identity = req.body.identity,
		token = req.body.token;

		jwt.verify(token, secret, (err, decoded) => {
			if (err) {
				res.status(401).send('invalid token');
			} else {
				res.send(generateToken(identity));
			}
		});
}
