const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const ClientCapability = require('twilio').jwt.ClientCapability;

const generateToken = function(identity, twilioToken, twilioSID, appSID) {
	const capability = new ClientCapability({
		accountSid: twilioSID,
		authToken: twilioToken
	});

	capability.addScope(new ClientCapability.IncomingClientScope(identity));
	capability.addScope(new ClientCapability.OutgoingClientScope({
		applicationSid: appSID,
		clientName: identity
	}));

	return {
		identity: identity,
		token: capability.toJwt()
	};
}


module.exports = (req, res) => {
	let identity = req.body.identity,
		token = req.body.token,
		twilioToken = req.body.twilioToken,
		twilioSID = req.body.twilioSID,
		appSID = req.body.appSID;

		jwt.verify(token, secret, (err, decoded) => {
			if (err) {
				res.status(401).send('invalid token');
			} else {
				res.send(generateToken(identity, twilioToken, twilioSID, appSID));
			}
		});
}
