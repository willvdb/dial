const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const twilioToken = require('../../api_config').twilioToken;
const twilioSID = require('../../api_config').twilioSID;
const appSID = require('../../api_config').appSID;
const VoiceResponse = require('twilio').twiml.VoiceResponse;

const isAValidPhoneNumber = function(number) {
	return /^[\d\+\-\(\) ]+$/.test(number);
}

const voiceResponse = function(toNumber, fromNumber) {
	console.log(toNumber, fromNumber);
	// Create a TwiML voice response
	const twiml = new VoiceResponse();

	if (toNumber) {
		// Wrap the phone number or client name in the appropriate TwiML verb
		// if is a valid phone number
		const attr = isAValidPhoneNumber(toNumber) ? 'number' : 'client';

		const dial = twiml.dial({
			callerId: fromNumber,
		});
		dial[attr]({}, toNumber);
	} else {
		twiml.say('Thanks for calling!');
	}

	return twiml.toString();
}


module.exports = (req, res) => {
	let toNumber = req.body.toNumber,
		fromNumber = req.body.fromNumber;

	res.set('Content-Type', 'text/xml');
	res.send(voiceResponse(toNumber, fromNumber));
}
