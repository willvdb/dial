const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const assignLeeds = function(userId, leedIds, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null);
		}
		let query = 'UPDATE leeds SET assigned_user=' + userId + ' WHERE id=ANY(\'{'
		for (var i = 0; i < leedIds.length; i++) {
			query += leedIds[i];
			if (i != leedIds.length - 1){
				query += ', ';
			}
		}
		query += '}\'::int[]);';

		client.query(query, function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			}

			cb(null, JSON.stringify(res));
		});
	});
}



module.exports = (req, res) => {
	let token = req.body.token;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).json({
				err: 'invalid token',
				success: false
			});
		}else{
			let leedIds = req.body.leedIds,
				userId = req.body.userId;

			assignLeeds(userId, leedIds, (err, result)=>{
				if (err){
					res.status(500).json({
						err: err,
						success: false
					});
				}

				res.status(200).json({
					result: result,
					success: true
				});
			});
		}
	});
}
