const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const addLeed = function(userId, leedData, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		client.query('INSERT INTO leeds (business, assigned_user, code, created_date, data) VALUES ($1, $2, $3, $4, $5);', ['bass', userId, 0, new Date(), leedData], function(err, res){
			if (err){
				cb(JSON.stringify(err), null);
			}

			cb(null, JSON.stringify(res));
		});
	});
}



module.exports = (req, res) => {
	let token = req.headers.authorization;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).send('invalid token');
		}else{
			//do stuff...
		}
	});
}
