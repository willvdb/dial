const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const multer = require('multer');
const fs = require('fs');
const pg = require('pg');
const phone = require('phone');
const csv=require('csvtojson')
const config = require('../../db_config');
const pool = new pg.Pool(config);
const upload = multer({
	dest: './uploads/',
	rename: function (fieldname, filename) {
		return new Date().toString();
	},
	onFileUploadStart: function (file) {
		console.log(file.originalname + ' is starting ...');
	},
	onFileUploadComplete: function (file) {
		console.log(file.fieldname + ' uploaded to  ' + file.path);
	}
}).single('file');

const addLeeds = function(leeds, business, campaign, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null)
		}

		leeds.forEach((leed) => {
				client.query('INSERT INTO leeds (business, code, phone, date_created, campaign, data) VALUES ($1, $2, $3, $4, $5, $6);', [business, 0, leed.phone[0], new Date(), campaign, leed], function(err, res){
					if (err){
						console.log(err);
					}
				});
		});

		cb(null, 'done');

	});
}

const parseLeeds = function(filePath, business, campaign, cb){
	let leeds = [];
	csv()
	.fromFile(filePath)
	.on('json',(jsonObj, x)=>{
		let formattedNumber = phone(jsonObj.Phone);
		if (formattedNumber.length){
			leeds.push({
				name: jsonObj.Name,
				phone: formattedNumber,
				email: jsonObj.Email
			});
		}
	})
	.on('done',()=>{
	    addLeeds(leeds, business, campaign, cb);
	});
}



module.exports = [(req, res) => {
	let business = req.query.business;
	let campaign = req.query.campaign;
	let token = req.headers.authorization;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).send('invalid token');
		}else{
			upload(req, res, function (err) {
				if (err) {
					return res.end(err.toString());
				}
				parseLeeds(req.file.path, business, campaign, (err, result) => {
					if (err){
						res.send(err);
					}
					res.send('successfully uploaded');
				});
			});
		}
	});
}]
