const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const url = require('url');
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const getLeeds = function(queryOptions, cb){
	pool.connect(function(err, client, done){
		if (err){
			done()
			cb(err, null);
		}

		let query = 'SELECT * FROM leeds WHERE ';
		let index = 0;
		Object.keys(queryOptions).forEach((key) => {
			if (index){
				query += ' AND '
			}
			index++;
			let temp = isNaN(queryOptions[key]) ? ( '\'' + queryOptions[key] + '\'') : queryOptions[key];
			query += key + '=' + temp;
		});
		query += ' AND assigned_user IS NULL;';

		client.query(query, function(err, res){
			if (err){
				console.log(err);
				done(err)
				cb(err, null);
			}
			done()
			cb(null, JSON.stringify(res.rows));
		});
	});
}

module.exports = (req, res) => {
	let url_parts = url.parse(req.url, true);
	let query = url_parts.query;
	let token = req.headers.authorization;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).send('invalid token');
		}else{
			getLeeds(query, (err, result) => {
				if (err){
					res.status(500).json(err);
				} else {
					res.status(200).json(result);
				}

			});
		}
	});
}
