const jwt = require('jsonwebtoken');
const secret = require('../../api_config').secret;
const pg = require('pg');
const config = require('../../db_config');
const pool = new pg.Pool(config);

const updateLeed = function(leedId, leedData, cb){
	pool.connect(function(err, client, done){
		if (err){
			cb(err, null);
		}
		let query = 'UPDATE leeds SET data=\'' + JSON.stringify(leedData).split('\'').join('') + '\' WHERE id=' + leedId + ';';

		client.query(query, function(err, res){
			if (err){
				cb(err, null);
			} else {
				cb(null, JSON.stringify(res));
			}
		});
	});
}



module.exports = (req, res) => {
	let token = req.body.token;
	jwt.verify(token, secret, (err, decoded) => {
		if (err){
			res.status(401).json({
				err: 'invalid token',
				success: false
			});
		}else{
			let leedId = req.body.id,
				leedData = req.body.data;

			updateLeed(leedId, leedData, (err, result)=>{
				if (err){
					res.status(500).json({
						err: err,
						success: false
					});
				} else {
					res.status(200).json({
						result: result,
						success: true
					});
				}

			});
		}
	});
}
