const express = require('express');
const router = express.Router();
const listLeeds = require('./routes/leed/list');
const listLeedDates = require('./routes/leed/list-dates');
const businessLeeds = require('./routes/leed/business');
const createLeed = require('./routes/leed/create');
const updateLeed = require('./routes/leed/update');
const uploadLeeds = require('./routes/leed/upload');
const assignLeeds = require('./routes/leed/assign');
const login = require('./routes/user/login');
const listUsers = require('./routes/user/list');
const register = require('./routes/user/register');
const updateUser = require('./routes/user/update');
const getBusinessData = require('./routes/business/get-data');
const listCampaigns = require('./routes/business/list-campaigns');
const updateBusiness = require('./routes/business/update');
const token = require('./routes/twilio/token');
const voice = require('./routes/twilio/voice');
const secret = 'W0IWhFPzQW8s';

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.use(function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', 'http://valor-software.github.io');
	res.setHeader('Access-Control-Allow-Methods', 'POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

//USER METHODS
router.get('/user/list', listUsers);

router.post('/user/login', login);

router.post('/user/register', register);

router.post('/user/update', updateUser);

//BUSINESS METHODS
router.get('/business/getData', getBusinessData)

router.get('/business/listCampaigns', listCampaigns)

router.post('/business/update', updateBusiness)

//LEED METHODS
router.get('/leed/list', listLeeds);

router.get('/leed/listDates', listLeedDates);

router.get('/leed/business', businessLeeds);

router.post('/leed/create', createLeed);

router.post('/leed/update', updateLeed);

router.post('/leed/upload', uploadLeeds);

router.post('/leed/assign', assignLeeds);


//TWILIO METHODS

router.post('/twilio/token', token);

router.post('/twilio/voice', voice);

module.exports = router;
