import { Component } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { AuthenticationService } from './_services/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

	constructor(private authenticationService: AuthenticationService) { }

	loginName() {
		if (this.authenticationService.token){
			return 'Logout'
		} else {
			return 'Login'
		}
	}

    isLoggedIn() {
        return !!this.authenticationService.token;
    }

    isAdmin() {
        return this.authenticationService.isAdmin;
    }
}
