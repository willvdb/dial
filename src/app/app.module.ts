import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { HttpModule, BaseRequestOptions } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { routes } from './app.routing';
import { AuthGuard, AdminGuard } from './_guards/index';
import { AuthenticationService, GlobalService,
		 RegistrationService, UserService, LeedService,
	 	 PhoneService, BusinessService } from './_services/index';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserinfoComponent } from './components/home/userinfo/userinfo.component';
import { LeedsComponent } from './components/leeds/leeds.component';
import { UploadComponent } from './components/upload/upload.component';
import { ManageComponent } from './components/manage/manage.component';
import { LeedComponent } from './components/leed/leed.component';
import { UserboxComponent } from './components/manage/userbox/userbox.component';
import { PhoneboxComponent } from './components/leeds/phonebox/phonebox.component';
import { PhoneLeedComponent } from './components/leeds/phoneleed/phoneleed.component';

@NgModule({
  declarations: [
	  AppComponent,
	  UserinfoComponent,
	  HomeComponent,
      LoginComponent,
	  LeedsComponent,
	  RegisterComponent,
	  UploadComponent,
	  ManageComponent,
	  LeedComponent,
	  UserboxComponent,
	  PhoneboxComponent,
	  PhoneLeedComponent
  ],
  imports: [
	  FileUploadModule,
	  BrowserModule,
	  FormsModule,
	  HttpModule,
	  TextMaskModule,
	  RouterModule.forRoot(routes)
  ],
  providers: [
	  AuthGuard,
	  AdminGuard,
	  AuthenticationService,
	  BusinessService,
	  GlobalService,
	  LeedService,
	  RegistrationService,
	  UserService,
	  PhoneService,
	  BaseRequestOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
