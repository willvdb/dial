import { Component, OnInit } from '@angular/core';
import { AuthenticationService, BusinessService } from '../../_services/index';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
	public uploader: FileUploader;
	public hasFileOverUploadZone: boolean;
	public currentUser: any;
	public campaign: string = '';
	public newCampaign: string = '';
	public campaignList: string[];

	constructor(
		private authenticationService: AuthenticationService,
		private businessService: BusinessService
	){
		this.currentUser = JSON.parse(localStorage.getItem('currentUser')).user;
		this.uploader = new FileUploader({url: '/api/leed/upload?business=' + this.currentUser.business + '&campaign=' + this.campaign , authToken: this.authenticationService.token});
		this.hasFileOverUploadZone = false;
	}

	public ngOnInit(){
    this.businessService.listCampaigns(this.currentUser.business).subscribe(campaigns => {
      this.campaignList = campaigns;
    })
	}

	public fileOverUploadZone(input: any){
		this.hasFileOverUploadZone = input;
	}

	public uploadFiles(){
		console.log("hello");
	}

	public updateUploader() {
		let campaignUpdate = (this.campaign === 'new') ? this.newCampaign : this.campaign
		this.uploader.options.url = '/api/leed/upload?business=' + this.currentUser.business + '&campaign=' + campaignUpdate
		console.log(this.uploader.options.url)
	}
}
