import { Component, Input } from '@angular/core';
import { Leed } from '../../_models/index';

@Component({
  selector: 'leed',
  templateUrl: './leed.component.html',
  styleUrls: ['./leed.component.scss']
})
export class LeedComponent {

	@Input() leed: Leed;

	selectMe(){
		this.leed.selected = this.leed.selected ? !this.leed.selected : true;
	}
}
