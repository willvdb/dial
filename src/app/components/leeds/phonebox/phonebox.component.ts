import { Component, Input, EventEmitter, Output, OnInit, OnChanges } from '@angular/core';
import { Leed } from '../../../_models/index';

import { PhoneService, LeedService } from '../../../_services/index';

declare const Twilio: any;

@Component({
  selector: 'phonebox',
  templateUrl: './phonebox.component.html',
  styleUrls: ['./phonebox.component.scss']
})
export class PhoneboxComponent implements OnInit {

	@Input() leed: Leed;
	@Input() twilioToken: any;
	noteIndex: number;
	number: String;
	currentUser: any;
	phoneMask: any = ['+', '1', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

	constructor(
		private phoneService: PhoneService,
		private leedService: LeedService) { }

	ngOnInit() {
		this.number = this.leed ? this.leed.phone.toString() : '';
		if (!this.leed.data.notes){
			this.leed.data.notes = []
		}
		this.noteIndex = this.leed.data.notes.length

		if (!this.leed.data.timesDialed){
			this.leed.data.timesDialed = 0;
		}

		this.currentUser = JSON.parse(localStorage.getItem('currentUser')).user;

	}

	makeCall() {
		console.log('HERE')
		let options = {
			toNumber: this.number,
			fromNumber: this.currentUser.data.number
		}
		this.leed.data.timesDialed ++;
		Twilio.Device.connect(options);
	}

	hangUp() {
		Twilio.Device.disconnectAll();
		this.updateLeed();
	}

	deleteLeed() {
		this.leed.data.timesDialed = 4;
		this.updateLeed();
	}

	updateLeed() {
		this.leedService.updateLeed(this.leed)
			.subscribe((success) => {
				if (success){
					this.leed.selected = false;
				} else {
					console.log('failed to update leed');
				}
			})
	}

	addNote() {
		this.leedService.updateLeed(this.leed)
			.subscribe((success) => {
				if (success){
					this.noteIndex++;
				} else {
					console.log('failed to update leed');
				}
			})
	}

	logLeed() {
		console.log(this.leed);
	}

}
