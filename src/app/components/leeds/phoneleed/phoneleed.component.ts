import { Component, Input } from '@angular/core';
import { Leed } from '../../../_models/index';

@Component({
  selector: 'phoneleed',
  templateUrl: './phoneleed.component.html',
  styleUrls: ['./phoneleed.component.scss']
})
export class PhoneLeedComponent {

	@Input() leed: Leed;
}
