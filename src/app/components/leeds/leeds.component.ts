import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { Leed } from '../../_models/index';
import { LeedService, PhoneService, BusinessService } from '../../_services/index';

declare const Twilio: any;

@Component({
	selector: 'leeds',
    templateUrl: 'leeds.component.html',
	styleUrls: ['./leeds.component.scss']
})

export class LeedsComponent implements OnInit {
    leeds: Leed[] = [];
	selectedLeed: Leed = null;
	business: any;
	currentUser: any;
	filterText: string = '';
	callingFlag: boolean = false;
	autoPlay: boolean = false;
	statusChecker = Observable.interval(500).subscribe(() => {
		if (this.autoPlay && Twilio && Twilio.Device) this.autoCallCheck();
	})

    constructor(
		private leedService: LeedService,
		private phoneService: PhoneService,
	 	private businessService: BusinessService) { }

    ngOnInit() {
		this.currentUser = JSON.parse(localStorage.getItem('currentUser')).user;
		let queryOptions = {
			'business': this.currentUser.business,
			'assigned_user': this.currentUser.id
		};
		this.businessService.getData(this.currentUser.business).subscribe(res => {
			this.business = res
			this.phoneService.getTwilioToken(this.business)
				.subscribe(function(token) {
					Twilio.Device.setup(token);
				})
		})
        this.leedService.getLeeds(queryOptions)
            .subscribe((leeds) => {
                this.leeds = leeds;
            });


		this.currentUser = JSON.parse(localStorage.getItem('currentUser')).user;
    }

	getLeeds() {
		return this.leeds.filter((leed) => {
			if (!leed.data.timesDialed) {
				leed.data.timesDialed = 0;
			}
			return (leed.data.name.toLowerCase().indexOf(this.filterText.toLowerCase()) > -1 && leed.data.timesDialed < 3);
		}).sort((leedOne, leedTwo) => {
			let numOne = leedOne.data.timesDialed ? leedOne.data.timesDialed : 0;
			let numTwo = leedTwo.data.timesDialed ? leedTwo.data.timesDialed : 0;
			return numOne - numTwo;
		})
	}

	selectLeed(leed){
		this.leeds.forEach((otherLeed) => {
			otherLeed.selected = false;
		})
		leed.selected = true;
	}'8015034200'

	play() {
		this.autoPlay = true;
	}

	pause() {
		this.autoPlay = false;
		this.callingFlag = true;
	}

	autoCallCheck() {
		//call is not curently happening
		if (Twilio.Device.status() === 'ready') {
			if (this.callingFlag) {
				 this.callingFlag = false;
			}
			else if (!this.callingFlag) {
				this.selectedLeed = this.getLeeds()[0];
				this.selectLeed(this.selectedLeed);
				this.callingFlag = true;
				this.makeCall(this.selectedLeed);
			}
		}
		//call is currently happening
		else if (Twilio.Device.status() === 'busy'){
			if (this.callingFlag) {
				this.callingFlag = false;
			}
		}
	}

	makeCall(leed) {
		leed.triggerCall = false;
		let options = {
			toNumber: leed.data.phone[0].toString(),
			fromNumber: this.currentUser.data.number
		}
		leed.data.timesDialed ++;
		Twilio.Device.connect(options);
	}

	hangUp(leed) {
		Twilio.Device.disconnectAll();
		this.updateLeed(leed);
	}

	deleteLeed(leed) {
		leed.data.timesDialed = 4;
		this.updateLeed(leed);
	}

	updateLeed(leed) {
		this.leedService.updateLeed(leed)
			.subscribe((success) => {
				if (success){
					leed.selected = false;
				} else {
					console.log('failed to update leed');
				}
			})
	}

}
