import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { User, Leed } from '../../../_models/index';
import { LeedService } from '../../../_services/index';
import { Router } from '@angular/router';
import { LeedComponent } from '../../leed/leed.component';

@Component({
  selector: 'userbox',
  templateUrl: './userbox.component.html',
  styleUrls: ['./userbox.component.scss']
})
export class UserboxComponent {

	@Input() user:User
	@Input() leedService: LeedService
	@Output() onAssigned = new EventEmitter<boolean>();
	leeds: Leed[] = []
	selectedLeeds: number[] = []

	constructor(private router: Router) { }

	ngOnInit(){
		let queryOptions = {
			'business': this.user.business,
			'assigned_user': this.user.id
		};
        this.leedService.getLeeds(queryOptions)
        .subscribe((leeds) => {
            this.leeds = leeds;
        });
	}

	setAssigned(){
		this.selectedLeeds = this.leeds.filter((leed)=>{
			return leed.selected;
		}).map((leed)=>{
			return leed.id;
		})
	}

	logAssigned(){
		console.log(this.selectedLeeds)
	}

	logUser(){
		this.ngOnInit();
	}

	assign(){
		let queryOptions = {
			userId: this.user.id,
			leedIds: this.leedService.leedsToAssign
		}
		this.leedService.assignLeedsToUser(queryOptions)
		.subscribe((result) => {
			this.onAssigned.emit(result);
		});
	}

}
