import { Component, OnInit } from '@angular/core';
import { Leed, User } from '../../_models/index';
import { LeedComponent } from '../leed/leed.component';
import { AuthenticationService, BusinessService, LeedService, UserService } from '../../_services/index';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {
    leeds: Leed[] = [];
	leedDates: string[] = [];
	campaignList: string[] = [];
	campaign: string = 'all';
	lastIndex: number = 0;
	selectedDate: string = 'all';
	users: User[] = [];
	filterText: string = '';

    constructor(
		private leedService: LeedService,
    	private businessService: BusinessService,
		private userService: UserService,
		private authenticationService: AuthenticationService) { }

    ngOnInit() {
		let currentUser = JSON.parse(localStorage.getItem('currentUser')).user;
		let queryOptions = {
			'business': currentUser.business
		};
		let leedQueryOptions = JSON.parse(JSON.stringify(queryOptions));
		if (this.selectedDate !== 'all') {
			leedQueryOptions['date_created'] = this.selectedDate;
		}
		if (this.campaign !== 'all') {
			leedQueryOptions['campaign'] = this.campaign;
		}


        this.leedService.getLeedsForBusinessOnly(leedQueryOptions)
            .subscribe((leeds) => {
                this.leeds = leeds;
            });

        this.leedService.getLeedDates(queryOptions)
            .subscribe((dates) => {
                this.leedDates = dates;
            });

		this.userService.getUsers(queryOptions)
			.subscribe((users)=>{
				this.users = users;
			})
		this.businessService.listCampaigns(currentUser.business)
			.subscribe((campaigns) => {
				this.campaignList = campaigns;
				console.log(campaigns)
			})
    }

	getLeeds(){
		return this.leeds.filter((leed) => {
			return (leed.data.name.toLowerCase().indexOf(this.filterText.toLowerCase()) > -1);
		}).sort((leedOne, leedTwo) => {
			let numOne = leedOne.data.timesDialed ? leedOne.data.timesDialed : 0;
			let numTwo = leedTwo.data.timesDialed ? leedTwo.data.timesDialed : 0;
			return numOne - numTwo;
		})
	}

	refreshAssignedList() {
		this.leedService.leedsToAssign = this.leeds.filter((leed)=>{
			return leed.selected;
		}).map((leed)=>{
			return leed.id;
		})
	}

	setAssigned(leed, event){
		let newIndex = this.getLeeds().indexOf(leed)
		if (event.shiftKey){
			for (let i = 0; i < this.getLeeds().length; i++){
				let leed = this.getLeeds()[i]
				if (i > this.lastIndex && i <= newIndex){
					leed.selected = true;
				}
			}
			this.refreshAssignedList()
		} else {
			this.refreshAssignedList()
		}

		this.lastIndex = newIndex;
	}

}
