import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService, RegistrationService } from '../../_services/index';

@Component({
    templateUrl: 'register.component.html',
	styleUrls: ['./register.component.scss']
})

export class RegisterComponent {
    model: any = {};
    loading = false;
    error = '';
	usernameError = false;
	passwordError = false;
	emailError = false;
	ccMask = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/ ]
	cvvMask = [/\d/, /\d/, /\d/, /\d/ ]

    constructor(
        private router: Router,
		private authenticationService: AuthenticationService,
        private registrationService: RegistrationService) { }

    register() {
		let self = this;
		this.error = 'that username is taken';
		this.usernameError = true;
		this.loading = false;

        this.registrationService.register(
			self.model.username.trim(),
			self.model.password.trim(),
			self.model.firstName.trim(),
			self.model.lastName.trim(),
			self.model.email.trim())
            .subscribe((result) => {
                if (result === true) {
                    let hash = localStorage.getItem('newHash');
					self.authenticationService.login(self.model.username.trim(), self.model.password.trim())
						.subscribe((result) => {
							if (result === true) {
								this.router.navigate(['/home']);
							} else {
								self.error = self.registrationService.error || 'Problem with one or more fields';
			                    self.loading = false;
							}
						})
                } else {
                    self.error = self.registrationService.error || 'Problem with one or more fields';
                    self.loading = false;
                }
            });
    }

	registerFacade() {
		let self = this;
		self.loading = true;
		setTimeout(function() {
			self.register()
		}, 1500)
	}

	usernameChange() {
		this.usernameError = false;
		this.error = '';
	}
}
