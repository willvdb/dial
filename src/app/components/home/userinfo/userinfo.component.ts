import { Component, Input } from '@angular/core';
import { User } from '../../../_models/index';

@Component({
	selector: 'user-info',
    templateUrl: 'userinfo.component.html',
	styleUrls: ['./userinfo.component.scss']
})

export class UserinfoComponent {

	@Input() user: User;

	logUser() {
		console.log(this.user);
	}

}
