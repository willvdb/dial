import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../_models/user';
import { GlobalService, UserService, BusinessService } from '../../_services/index';
import { UserinfoComponent } from "./userinfo/userinfo.component";

@Component({
    templateUrl: 'home.component.html',
	styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
	currentUser: User;
	business: any;
	editMode: boolean = false
	businessEditMode: boolean = false
	phoneMask: any = ['+', '1', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]


    constructor(private router: Router, private userService: UserService, private businessService: BusinessService) { }

	ngOnInit() {
		let temp = JSON.parse(localStorage.getItem('currentUser'));
		if (!temp){
			this.router.navigate(['/login']);
		}
		this.currentUser = {
			'id': temp.user.id,
			'username': temp.user.username,
			'business': temp.user.business,
			'data': temp.user.data
		}
		this.businessService.getData(this.currentUser.business).subscribe(res => {
			this.business = res
		})
	}

	isAdmin(): boolean {
		return this.currentUser && this.currentUser.data.role === 'admin'
	}

	logUser() {
		console.log(this.currentUser);
	}

	toggleEdit() {
		this.editMode = !this.editMode;
	}

	cancelEdit() {
		this.ngOnInit();
		this.toggleEdit();
	}

	updateUser() {
		this.userService.updateUser(this.currentUser.id, this.currentUser.data).subscribe((user) => {
			this.currentUser = user;
			this.toggleEdit();
		})
	}

	toggleBusinessEdit() {
		this.businessEditMode = !this.businessEditMode;
	}

	cancelBusinessEdit() {
		this.ngOnInit();
		this.toggleBusinessEdit();
	}

	updateBusiness() {
		this.businessService.updateBusiness(this.currentUser.business, this.business).subscribe((res) => {
			if (!res){
				console.log('failure to update...');
			}
			this.toggleBusinessEdit();
		})
	}

}
