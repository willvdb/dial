import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private router: Router) { }

	canActivate() {
		if (localStorage.getItem('currentUser')) {
			let user = JSON.parse(localStorage.getItem('currentUser')).user;
			if (user.data.role && user.data.role === 'admin'){
				return true;
			}
		}

        // not logged in so redirect to login page
        this.router.navigate(['/home']);
        return false;
    }
}
