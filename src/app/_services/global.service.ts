import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../_models/user';

@Injectable()
export class GlobalService {
    private _currentUser: User;

    constructor() {

    }

	init() {

	}

	set currentUser(newUser: User){
		this._currentUser = newUser;
	}

	get currentUser(){
		return this._currentUser;
	}

}
