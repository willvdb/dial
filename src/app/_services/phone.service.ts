import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from '../_services/index';

@Injectable()
export class PhoneService {

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

	initiateCall(): Observable<any> {
		return this.http.post('/api/twilio/token', {'token': this.authenticationService.token})
            .map((response: Response) => {
				return response.json();
			});
	}

	getTwilioToken(business): Observable<any> {
		business['token'] = this.authenticationService.token
		console.log(business)
		return this.http.post('/api/twilio/token', business)
            .map((response: Response) => {
				return response.json().token;
			});
	}
}
