import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class RegistrationService {
    public error: string;

    constructor(private http: Http) { }

	init() {
		let self = this;
	}

    register(username: string, password: string, firstName: string, lastName: string, email: string): Observable<boolean> {
        return this.http.post('/api/user/register', {
			username: username,
			password: password,
			business: 'brady',
			data: {
				firstName: firstName,
				lastName: lastName,
				email: email,
				role: 'employee'
			} })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                if (response.json().success) {

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('newHash', response.json().hash);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
					this.error = response.json().msg;
                    return false;
                }
            });
    }
}
