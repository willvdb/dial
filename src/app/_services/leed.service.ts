import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from '../_services/index';
import { Leed } from '../_models/index';

@Injectable()
export class LeedService {

	public leedsToAssign?: number[] = [];

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getLeeds(queryString: any): Observable<Leed[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
		let url = '/api/leed/list?';
		Object.keys(queryString).forEach((key) => {
		    url += key + '=' + queryString[key] + '&';
		});

        // get products from api
        return this.http.get(url, options)
            .map((response: Response) => {
				let leeds: Leed[] = [];
                let tempLeeds = JSON.parse(response.json());
				tempLeeds.forEach(leed => {
				    leeds.push(leed);
				});
				return leeds;
			});
    }

	getLeedDates(queryString: any): Observable<string[]> {
		// add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
		let url = '/api/leed/listDates?';
		Object.keys(queryString).forEach((key) => {
		    url += key + '=' + queryString[key] + '&';
		});

        // get products from api
        return this.http.get(url, options)
            .map((response: Response) => {
				let dates: string[] = [];
                let tempLeeds = JSON.parse(response.json());
				tempLeeds.forEach(dateObj => {
					let dateString = new Date(dateObj['date_created']).toDateString()
				    dates.push(dateString);
				});
				return dates;
			});
	}

    getLeedsForBusinessOnly(queryString: any): Observable<Leed[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
		let url = '/api/leed/business?';
		Object.keys(queryString).forEach((key) => {
		    url += key + '=' + queryString[key] + '&';
		});

        // get products from api
        return this.http.get(url, options)
            .map((response: Response) => {
				let leeds: Leed[] = [];
                let tempLeeds = JSON.parse(response.json());
				tempLeeds.forEach(leed => {
				    leeds.push(leed);
				});
				return leeds;
			});
    }

	assignLeedsToUser(options: any): Observable<boolean> {
		options['token'] = this.authenticationService.token;
		return this.http.post('api/leed/assign', options)
		.map((response: Response) => {
			console.log(response);
			return response.json().success;
		});
	}

	updateLeed(options: any): Observable<boolean> {
		options['token'] = this.authenticationService.token;
		return this.http.post('api/leed/update', options)
		.map((response: Response) => {
			console.log(response);
			return response.json().success;
		});
	}
}
