import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService, GlobalService } from '../_services/index';
import { User } from '../_models/index';

@Injectable()
export class BusinessService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
		private globalService: GlobalService) {
    }

	getData(businessName: string): Observable<any> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
		let url = '/api/business/getData?business=' + businessName;

        // get business data from api
        return this.http.get(url, options).map((response: Response) => {
			return JSON.parse(response.json()).data
		});
    }

	listCampaigns(businessName: string): Observable<string[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
		let url = '/api/business/listCampaigns?business=' + businessName;

        // get business data from api
        return this.http.get(url, options).map((response: Response) => {
			return JSON.parse(response.json()).map(campaignObj => {
        return campaignObj.campaign;
      })
		});
    }

	updateBusiness(businessName: string, data: any): Observable<boolean> {
		return this.http.post('/api/business/update', {
			'token': this.authenticationService.token,
			'name': businessName,
			'data': data
		}).map((response: Response) => {
			return Observable.of(true)
		}).catch(err => {
			return Observable.of(false)
		})
	}
}
