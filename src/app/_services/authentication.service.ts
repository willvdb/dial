import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { GlobalService }  from "./global.service";
import { User } from '../_models/index';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public token: string;
	public isAdmin: boolean;

    constructor(private http: Http, private globalService: GlobalService) {
        // set token if saved in local storage
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
		this.isAdmin = currentUser && currentUser.user &&
					   currentUser.user.data &&
					   currentUser.user.data.role === 'admin';
    }

	init() {}

    login(username: string, password: string): Observable<boolean> {
        return this.http.post('/api/user/login', { username: username, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;
					let user = response.json() && response.json().user;
					this.isAdmin = user.data && user.data.role === 'admin';
					delete user.hash;

					//set the global user variable
					this.globalService.currentUser = {
						id: user.id,
						username: user.username,
						business: user.business,
						data: user.data
					};

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ user: user, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
		this.isAdmin = false;
        localStorage.removeItem('currentUser');
    }
}
