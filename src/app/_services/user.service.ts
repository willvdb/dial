import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService, GlobalService } from '../_services/index';
import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
		private globalService: GlobalService) {
    }

	getUsers(queryString: any): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
    		let url = '/api/user/list?';
    		Object.keys(queryString).forEach((key) => {
    		    url += key + '=' + queryString[key] + '&';
    		});

        // get products from api
        return this.http.get(url, options)
            .map((response: Response) => {
				let users: User[] = [];
                let tempUsers = JSON.parse(response.json());
				tempUsers.forEach(user => {
				    users.push(user);
				});
				return users;
			});
    }

	updateUser(userId: number, data: any): Observable<User> {
		return this.http.post('/api/user/update', {
			'token': this.authenticationService.token,
			'id': userId,
			'data': data
		})
            .map((response: Response) => {
				//get the old user and update it
				let temp = JSON.parse(localStorage.getItem('currentUser'));

				//set the global user variable
				temp.user.data = data;

				// store username and jwt token in local storage to keep user logged in between page refreshes
				localStorage.setItem('currentUser', JSON.stringify(temp));
				return temp.user
			});
	}
}
