export interface Leed {
	id: number;
	business: string;
	assignedUser: number;
	code: number;
	dateCreated: string;
	name: string;
	email: string;
	phone: string[];
	data?: any;
	selected?: boolean;
}
