export interface User {
	id: number;
    username: string;
	business: string;
	data: any;
}
