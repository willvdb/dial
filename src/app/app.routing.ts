// app.routes.ts
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UploadComponent } from './components/upload/upload.component';
import { ManageComponent } from './components/manage/manage.component';
import { LeedsComponent } from './components/leeds/leeds.component';
import { AuthGuard, AdminGuard } from './_guards/index';

export const routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
  	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  	{ path: 'upload', component: UploadComponent, canActivate: [AuthGuard, AdminGuard] },
  	{ path: 'manage', component: ManageComponent, canActivate: [AuthGuard, AdminGuard] },
  	{ path: 'leeds', component: LeedsComponent, canActivate: [AuthGuard] },

	{ path: '**', redirectTo: 'home' }
];
